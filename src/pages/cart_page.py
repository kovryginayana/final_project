from tests.constans import URLs


class CartPage:

    def __init__(self, page):
        self.page = page
        self.place_order_button = page.get_by_role("button", name="Place Order")
        self.product_title = page.locator("td")
        self.delete_button = page.locator("//a[text()='Delete']")
        self.purchase_button = page.get_by_role("button", name="Purchase")
        self.place_order_title = page.locator("h5#orderModalLabel.modal-title")
        self.name = page.get_by_label("Total:")
        self.country = page.get_by_label("Country:")
        self.city = page.get_by_label("City:")
        self.card = page.get_by_label("Credit card:")
        self.month = page.get_by_label("Month:")
        self.year = page.get_by_label("Year:")
        self.pop_up = page.expect_event('dialog')
        self.thank_you = page.get_by_role("heading", name="Thank you for your purchase!")
        self.ok_button = page.get_by_role("button", name="OK")

    def open_cart(self):
        self.page.goto(URLs.CART)

    def delete_item(self):
        self.page.goto(URLs.CART)
        self.page.wait_for_timeout(500)
        self.delete_button.nth(0).click()
        self.page.wait_for_timeout(800)
        assert self.delete_button.is_hidden()

    def place_order_all_fields(self):
        self.place_order_button.click()
        assert self.place_order_title.nth(0).inner_text() == "Place order"
        self.name.fill("Delulu")
        self.country.fill("Georgia")
        self.city.fill("Tbilisi")
        self.card.fill("3768 7686 4426 894")
        self.month.fill("01")
        self.year.fill("2024")
        self.purchase_button.click()

    def place_order_mandatory_fields(self):
        self.place_order_button.click()
        assert self.place_order_title.nth(0).inner_text() == "Place order"
        self.name.fill("Meow")
        self.card.fill("0144 7686 4426 894")
        self.purchase_button.click()

    def place_order_empty_fields(self):
        self.place_order_button.click()
        assert self.place_order_title.nth(0).inner_text() == "Place order"
        assert self.thank_you.is_hidden()

    def confirmation_window(self):
        assert self.thank_you.is_visible()
        self.ok_button.click()
        self.page.wait_for_timeout(500)
        assert self.thank_you.is_hidden()