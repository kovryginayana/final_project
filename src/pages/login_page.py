from tests.constans import URLs


class LoginPage:
    def __init__(self, page):
        self.page = page
        self.login_open = page.get_by_role("link", name="Log in")
        self.username = page.locator("#loginusername")
        self.password = page.locator("#loginpassword")
        self.login_header = page.locator("h5#logInModalLabel.modal-title")
        self.login_button = page.get_by_role("button", name="Log in")
        self.welcome = page.locator("a#nameofuser.nav-link")
        self.logout_button = page.get_by_role("link", name="Log out")
        self.sign_up_button = page.locator("a#signin2.nav-link")
        self.close_button = page.locator("#logInModal").get_by_text("Close")

    def login(self, page, username, password):
        page.goto(URLs.MAIN)
        self.login_open.click()
        assert self.login_header.inner_text() == "Log in"
        self.username.fill(username)
        self.password.fill(password)

    def login_success_state(self):
        self.login_button.click()
        self.welcome.click()
        assert self.welcome.inner_text() == "Welcome username+201"

    def login_wrong_password(self):
        with self.page.expect_event('dialog') as dialog_info:
            self.login_button.click()
        assert dialog_info.value.message == "Wrong password."

    def login_empty_field(self):
        with self.page.expect_event('dialog') as dialog_info:
            self.login_button.click()
        self.page.wait_for_timeout(500)
        assert dialog_info.value.message == "Please fill out Username and Password."

    def login_close(self):
        self.page.goto(URLs.MAIN)
        self.login_open.click()
        self.close_button.click()
        self.page.wait_for_timeout(500)
        assert self.login_header.is_hidden()

    def logout_state(self):
        self.login_button.click()
        self.logout_button.click()
        assert self.sign_up_button.is_visible()