from tests.constans import URLs


class ProductPage:
    def __init__(self, page):
        self.page = page
        self.item = page.locator("a.hrefch")
        self.product_name = page.locator("h2.name")
        self.add_to_cart_button = page.get_by_role("link", name="Add to cart")
        self.pop_up = page.expect_event('dialog')

    def open_product_page(self, i):
        self.item.nth(i).click()
        self.page.wait_for_timeout(500)

    def item_title_product_page(self):
        self.product_name.nth(0).inner_text()

    def open_and_add_item(self, i):
        self.page.goto(URLs.MAIN)
        self.item.nth(i).click()
        self.page.wait_for_timeout(500)
        self.add_to_cart_button.click()

    def add_item_to_cart(self):
        with self.pop_up as dialog_info:
            self.add_to_cart_button.click()
        assert dialog_info.value.message == "Product added"