from tests.constans import URLs


class ContactUsPage:
    def __init__(self, page):
        self.page = page
        self.contact_us_open = page.get_by_role("link", name="Contact")
        self.new_message = page.locator("h5#exampleModalLabel.modal-title")
        self.email = page.locator("input#recipient-email.form-control")
        self.name = page.locator("input#recipient-name.form-control")
        self.message = page.get_by_label("Message:")
        self.send_button = page.get_by_role("button", name="Send message")
        self.close_button = page.get_by_label("New message").get_by_text("Close")

    def contact_us(self, page):
        page.goto(URLs.MAIN)
        self.contact_us_open.click()
        assert self.new_message.inner_text() == "New message"
        self.email.fill("email.com")
        self.name.fill("name")
        self.message.fill("h e l p")
        assert self.page.url == URLs.MAIN

    def contact_us_close(self, page):
        page.goto(URLs.MAIN)
        self.contact_us_open.click()
        self.close_button.click()
        self.page.wait_for_timeout(500)
        assert self.new_message.is_hidden()