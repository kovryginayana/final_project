import random

from tests.constans import URLs


class SignUpPage:
    def __init__(self, page):
        self.page = page
        self.sign_up_open = page.get_by_role("link", name="Sign up")
        self.username = page.get_by_label("Username:")
        self.password = page.get_by_label("Password:")
        self.sign_up_button = page.get_by_role("button", name="Sign up")
        self.sign_up_header = page.locator("h5#signInModalLabel.modal-title")
        self.close_button = page.get_by_label("Sign up").get_by_text("Close")

    def sign_up(self, page, username):
        page.goto(URLs.MAIN)
        self.sign_up_open.click()
        assert self.sign_up_header.inner_text() == "Sign up"
        self.username.fill(username)
        self.password.fill("password")

    def sign_up_success(self):
        with self.page.expect_event('dialog') as dialog_info:
            self.sign_up_button.click()
        self.page.wait_for_timeout(1000)
        assert dialog_info.value.message == "Sign up successful."

    def sign_up_user_exist(self):
        with self.page.expect_event('dialog') as dialog_info:
            self.sign_up_button.click()
        assert dialog_info.value.message == "This user already exist."

    def sign_up_empty_field(self):
        with self.page.expect_event('dialog') as dialog_info:
            self.sign_up_button.click()
        assert dialog_info.value.message == "Please fill out Username and Password."

    def sign_up_close(self):
        self.page.goto(URLs.MAIN)
        self.sign_up_open.click()
        self.close_button.click()
        self.page.wait_for_timeout(500)
        assert self.sign_up_header.is_hidden()