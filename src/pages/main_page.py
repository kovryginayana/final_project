from tests.constans import URLs


class MainPage:

    def __init__(self, page):
        self.page = page
        self.phones = page.get_by_role("link", name="Phones")
        self.laptops = page.get_by_role("link", name="Laptops")
        self.monitors = page.get_by_role("link", name="Monitors")
        self.item = page.locator("a.hrefch")

    def open(self, page):
        page.goto(URLs.MAIN)

    def open_phones(self, page):
        page.goto(URLs.MAIN)
        self.phones.click()
        self.page.wait_for_timeout(1000)

    def open_laptops(self, page):
        page.goto(URLs.MAIN)
        self.laptops.click()
        self.page.wait_for_timeout(1000)

    def open_monitors(self, page):
        page.goto(URLs.MAIN)
        self.monitors.click()
        self.page.wait_for_timeout(1000)

    def item_title_main_page(self, i):
        self.item.nth(i).inner_text()