import pytest
from playwright.sync_api import sync_playwright


@pytest.fixture(autouse=True)
def browser():
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True)
        yield browser
        browser.close()


@pytest.fixture(autouse=True)
def page(browser):
    return browser.new_page()