import random
from src.pages.sign_up_page import SignUpPage


class TestSignUp:
    def test_sign_up(self, page):
        sign_up = SignUpPage(page)

        sign_up.sign_up(page, username=f'username+{random.randint(300, 1000)}')
        sign_up.sign_up_success()

    def test_sign_up_exists(self, page):
        sign_up = SignUpPage(page)

        sign_up.sign_up(page, username="username+201")
        sign_up.sign_up_user_exist()

    def test_sign_up_close(self, page):
        sign_up = SignUpPage(page)

        sign_up.sign_up_close()

    # тут есть тест на пустые поля, но у меня не получилось, диалоговое окно не закрывается и тест падает с TimeOut

    # def test_sign_up_empty_field(self, page):
    #     sign_up = SignUpPage(page)

    #     sign_up.sign_up(page, username="")
    #     sign_up.sign_up_empty_field()