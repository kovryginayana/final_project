from src.pages.cart_page import CartPage
from src.pages.main_page import MainPage
from src.pages.product_page import ProductPage
from tests.constans import URLs


class TestCartAndOrder:

    def test_item_titles_match(self, page):
        i = 1
        main_page = MainPage(page)
        product_page = ProductPage(page)

        main_page.open(page)
        item_title = main_page.item_title_main_page(i)
        product_page.open_product_page(i)
        product_title = product_page.item_title_product_page()
        assert item_title == product_title

    def test_delete_item(self, page):
        i = 0
        product_page = ProductPage(page)
        cart_page = CartPage(page)

        product_page.open_and_add_item(i)
        cart_page.delete_item()

    def test_place_order_empty_fields(self, page):
        i = 0
        product_page = ProductPage(page)
        cart_page = CartPage(page)

        product_page.open_and_add_item(i)
        cart_page.open_cart()
        cart_page.place_order_empty_fields()

    def test_place_order_success(self, page):
        i = 0
        product_page = ProductPage(page)
        cart_page = CartPage(page)

        product_page.open_and_add_item(i)
        cart_page.open_cart()
        cart_page.place_order_all_fields()

    def test_place_order_mandatory_fields(self, page):
        i = 1
        product_page = ProductPage(page)
        cart_page = CartPage(page)

        product_page.open_and_add_item(i)
        cart_page.open_cart()
        cart_page.place_order_mandatory_fields()

    def test_confirmation_window(self, page):
        i = 0
        product_page = ProductPage(page)
        cart_page = CartPage(page)

        product_page.open_and_add_item(i)
        cart_page.open_cart()
        cart_page.place_order_mandatory_fields()
        cart_page.confirmation_window()