from src.pages.login_page import LoginPage


class TestLogin:

    def test_login_wrong_password(self, page):
        login = LoginPage(page)

        login.login(page, username="username+201", password="p")
        login.login_wrong_password()

    def test_login_close(self, page):
        login = LoginPage(page)

        login.login_close()

    def test_login_success(self, page):
        login = LoginPage(page)

        login.login(page, username="username+201", password="password")
        login.login_success_state()

    # тут есть тест на пустые поля, но у меня не получилось, диалоговое окно не закрывается и тест падает с TimeOut
    #
    # def test_login_empty_field(self, page):
    #     login_page = LoginPage(page)
    #
    #     login_page.login(page, username="", password="")
    #     login_page.login_empty_field()

    def test_logout(self, page):
        login = LoginPage(page)

        login.login(page, username="username+201", password="password")
        login.logout_state()