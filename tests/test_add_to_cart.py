from src.pages.main_page import MainPage
from src.pages.product_page import ProductPage


class TestAddToCart:

    def test_add_phone_to_cart(self, page):
        i = 1
        main_page = MainPage(page)
        product_page = ProductPage(page)

        main_page.open_phones(page)
        product_page.open_product_page(i)
        product_page.add_item_to_cart()

    def test_add_laptop_to_cart(self, page):
        i = 1
        main_page = MainPage(page)
        product_page = ProductPage(page)

        main_page.open_laptops(page)
        product_page.open_product_page(i)
        product_page.add_item_to_cart()

    def test_add_monitor_to_cart(self, page):
        i = 0
        main_page = MainPage(page)
        product_page = ProductPage(page)

        main_page.open_monitors(page)
        product_page.open_product_page(i)
        product_page.add_item_to_cart()