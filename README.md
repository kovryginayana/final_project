# Test-plan
Tests:

1. Sign up
   - with valid credentials 
   - with credentials of the existing user
2. Login 
   - with valid credentials
   - with wrong password
3. Logout
4. Product page
5. Add phone to cart
6. Add laptop to cart
7. Add monitor to cart
8. Place order
   - all mandatory fields are filled
   - empty Name/Card
9. Delete item from Cart 
10. Contact us form
11. Close modals
------

**18 tests :)**
